<?php
declare(strict_types=1);

namespace App\Application\Services;

use App\Domain\Words\CommonWordRepository;

class WordCounter
{
    const MOST_FREQUENT_WORD_COUNT = 10;

    /** @var CommonWordRepository */
    protected $commonWordRepository;

    /** @var array */
    protected $mostCommonWordsInEnglish;

    /**
     * WordCounter constructor.
     * @param CommonWordRepository $commonWordRepository
     */
    public function __construct(CommonWordRepository $commonWordRepository)
    {
        $this->commonWordRepository = $commonWordRepository;
        $this->mostCommonWordsInEnglish = $commonWordRepository->getMostCommonWords();
    }

    /**
     * @param array $feed
     * @return array
     */
    public function getMostFrequentWords(array $feed): array
    {
        $commonWords = $this->getAllWordsArray($feed['entry']);
        $commonWords = array_map(function ($value) {
            return trim($value, " \t\n\r\0\x0B.,-:;");
        }, $commonWords);

        $commonWords = array_filter($commonWords, function($value) {
            return $value !== ''
                && !is_numeric($value)
                && strlen($value) > 1
                && strlen($value) === strlen(utf8_decode($value));
        });

        $commonWords = array_count_values($commonWords);
        $commonWords = array_diff_key($commonWords, array_flip($this->mostCommonWordsInEnglish));
        arsort($commonWords);
        $commonWords = array_slice($commonWords, 0, self::MOST_FREQUENT_WORD_COUNT);

        return $commonWords;
    }

    /**
     * @param array $feed
     * @return array
     */
    private function getAllWordsArray(array $feed): array
    {
        $words = [];
        $delimiters = '/[\s]/';

        foreach ($feed as $key => $feedItem) {
            if (is_array($feedItem)) {
                $words = array_merge($words, $this->getAllWordsArray($feedItem));
                continue;
            }

            if (!in_array($key, ['summary', 'title', 'name'])) {
                continue;
            }

            $feedItem = strip_tags($feedItem);
            $words = array_merge($words, preg_split($delimiters, $feedItem));
        }

        return $words;
    }
}