<?php
declare(strict_types=1);

namespace App\Application\Middleware;

use App\Domain\UserToken\UserTokenRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Exception\HttpUnauthorizedException;

class TokenAuthorizationMiddleware implements Middleware
{

    /** @var UserTokenRepository */
    protected $userTokenRepository;

    public function __construct(UserTokenRepository $userTokenRepository)
    {
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        $token = $request->getHeader('Authorization');

        if (empty($token) || !$this->userTokenRepository->getUserByToken($token[0])) {
            throw new HttpUnauthorizedException($request);
        }

        return $handler->handle($request);
    }
}