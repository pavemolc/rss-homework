<?php
declare(strict_types=1);

namespace App\Domain\User;

interface UserRepository
{
    /**
     * @param string $email
     * @return array|null
     */
    public function findByEmail(string $email): ?User;

    /**
     * @param string $email
     * @return bool
     */
    public function userEmailExists(string $email): bool;

    /**
     * @param string $email
     * @param string $passwordHash
     * @return bool
     * @throws UserNotFoundException
     */
    public function createUser(string $email, string $passwordHash): bool;
}
