<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Feed;

use App\Domain\Feed\FeedRepository;

class InMemoryRssFeedRepository implements FeedRepository
{
    /**
     * @return array
     */
    public function getFeed(): array
    {
        $fileContents = file_get_contents('https://www.theregister.co.uk/software/headlines.atom');
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents);
        $rssFeed = json_encode($simpleXml);
        $rssFeed = json_decode($rssFeed, true);

        return $rssFeed;
    }
}