<?php
declare(strict_types=1);

use App\Application\Actions\Feed\FeedAction;
use App\Application\Actions\User\AuthenticateUserAction;
use App\Application\Actions\User\RegisterUserAction;
use App\Application\Actions\User\ValidateEmailAction;
use App\Application\Middleware\TokenAuthorizationMiddleware;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->group('/api', function (Group $group) {

        $group->get('/get-rss-feed', FeedAction::class)->add(TokenAuthorizationMiddleware::class);

        $group->group('/user', function (Group $userGroup) {
            $userGroup->post('/register', RegisterUserAction::class);
            $userGroup->post('/validate-email', ValidateEmailAction::class);
            $userGroup->post('/login', AuthenticateUserAction::class);

        });
    });
};
