<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use App\Application\Actions\ActionPayload;
use App\Application\Services\TokenGenerator;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\UserToken\UserTokenRepository;
use DI\Container;
use Tests\TestCase;

class AuthenticateUserActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();
        $testEmail = 'qwe@qwe.qwe';
        $testToken = 'my_test_token';

        /** @var Container $container */
        $container = $app->getContainer();

        $user = new User();
        $user->email = $testEmail;
        $user->password_hash = '$2y$10$u6tPoqLPznIaLL8gIvRbO.iv4i4Ucibf5MXjpbTW7IDSX/YVMtBbC';

        $userRepositoryProphecy = $this->prophesize(UserRepository::class);
        $userRepositoryProphecy
            ->findByEmail($testEmail)
            ->willReturn($user)
            ->shouldBeCalledOnce();

        $tokenGeneratorProphecy = $this->prophesize(TokenGenerator::class);
        $tokenGeneratorProphecy
            ->generateToken()
            ->willReturn($testToken)
            ->shouldBeCalledOnce();

        $userTokenRepositoryProphecy = $this->prophesize(UserTokenRepository::class);
        $userTokenRepositoryProphecy
            ->createUserToken($user, $testToken)
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $container->set(UserRepository::class, $userRepositoryProphecy->reveal());
        $container->set(TokenGenerator::class, $tokenGeneratorProphecy->reveal());
        $container->set(UserTokenRepository::class, $userTokenRepositoryProphecy->reveal());

        $request = $this->createRequest('POST', '/api/user/login', [
            'email' => 'qwe@qwe.qwe',
            'password' => 'qweqwe'
        ]);
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, [
            'apiToken' => 'my_test_token'
        ]);

        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}