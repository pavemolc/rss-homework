
const registrationForm = Vue.component('registration-form', {
    template: '#registration-template',

    data: function () {
        return {
            email: null,
            password: null,
            errorText: null,
            successText: null
        }
    },

    mounted () {
        if ($cookies.get('apiToken')) {
            router.push('/rss-feed');
        }
    },

    methods: {
        register(e) {
            e.preventDefault();
            axios
                .post('/api/user/register', {
                    email: this.email,
                    password: this.password
                })
                .then(response => {
                    this.errorText = null;

                    if (response.data.data.registered) {
                        this.successText = 'Email ' + ' is registered. Now go to the Login page and enter credentials to authorize'
                    }
                })
                .catch(error => {
                    this.errorText = error.response.data.error.description;
                    this.successText = null;
                });
        },

        validateEmail() {
            axios
                .post('/api/user/validate-email', {
                    email: this.email
                })
                .then(response => {
                    this.errorText = null;
                })
                .catch(error => {
                    this.errorText = error.response.data.error.description;
                    this.successText = null;
                });
        },
    }
});

const loginForm = Vue.component('login-form', {
    template: '#login-template',

    data: function () {
        return {
            email: null,
            password: null,
            errorText: null
        }
    },

    mounted () {
        if ($cookies.get('apiToken')) {
            router.push('/rss-feed');
        }
    },

    methods: {
        login(e) {
            e.preventDefault();
            axios
                .post('/api/user/login', {
                    email: this.email,
                    password: this.password
                })
                .then(response => {
                    if (response.data.data.apiToken) {
                        this.errorText = null;
                        $cookies.set('apiToken', response.data.data.apiToken, '1d');
                        window.location.reload();
                        router.push('/rss-feed');
                    } else {
                        this.errorText = response.data.errorText;
                    }
                }).catch(error => {
                    this.errorText = error.response.data.error.description;
                });
        }
    }
});

const rssFeed = Vue.component('rss-feed', {
    template: '#rss-feed-template',

    data: function () {
        return {
            rssFeed: {},
            mostFrequentWords: {}
        }
    },

    computed: {
        showFeed() {
            return Object.keys(this.rssFeed).length > 0;
        },
        isLoggedIn() {
            return $cookies.isKey('apiToken');
        }
    },

    mounted () {
        if (!this.isLoggedIn) {
            router.push('/login');
            return;
        }

        this.getFeed();
    },

    methods: {
        getFeed() {
            let token = $cookies.get('apiToken');

            if (!token) {
                return;
            }

            axios
                .get('/api/get-rss-feed', {
                    'headers' : {
                        Authorization: token
                    }
                })
                .then(response => {
                    this.rssFeed = response.data.data.rssFeed;
                    this.mostFrequentWords = response.data.data.mostFrequentWords;
                });
        },

        logout() {
            $cookies.remove('apiToken');
            window.location.reload();
            router.push('/login');
        }
    }
});