<?php
declare(strict_types=1);

namespace App\Domain\Words;


interface CommonWordRepository
{
    /**
     * @return array
     */
    public function getMostCommonWords(): array;
}