<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use App\Application\Actions\ActionPayload;
use App\Application\Services\TokenGenerator;
use App\Domain\User\UserRepository;
use DI\Container;
use Tests\TestCase;

class RegisterUserActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();
        $testEmail = 'qwe@qwe.qwe';
        $testPassword = 'qweqwe';
        $testHash = '$2y$10$u6tPoqLPznIaLL8gIvRbO.iv4i4Ucibf5MXjpbTW7IDSX/YVMtBbC';

        /** @var Container $container */
        $container = $app->getContainer();

        $userRepositoryProphecy = $this->prophesize(UserRepository::class);
        $userRepositoryProphecy
            ->userEmailExists($testEmail)
            ->willReturn(false)
            ->shouldBeCalledOnce();

        $userRepositoryProphecy
            ->createUser($testEmail, $testHash)
            ->willReturn(false)
            ->shouldBeCalledOnce();

        $tokenGeneratorProphecy = $this->prophesize(TokenGenerator::class);
        $tokenGeneratorProphecy
            ->generatePasswordHash($testPassword)
            ->willReturn($testHash)
            ->shouldBeCalledOnce();

        $container->set(UserRepository::class, $userRepositoryProphecy->reveal());
        $container->set(TokenGenerator::class, $tokenGeneratorProphecy->reveal());

        $request = $this->createRequest('POST', '/api/user/register', [
            'email' => 'qwe@qwe.qwe',
            'password' => $testPassword
        ]);

        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, [
            'registered' => true
        ]);

        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}