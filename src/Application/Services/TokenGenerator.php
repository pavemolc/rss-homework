<?php
declare(strict_types=1);

namespace App\Application\Services;

class TokenGenerator
{
    const RANDOM_BYTES_LENGTH = 72;

    /**
     * @return string
     * @throws \Exception
     */
    public function generateToken(): string
    {
        return bin2hex(random_bytes(self::RANDOM_BYTES_LENGTH));
    }

    /**
     * @param string $password
     * @return string
     */
    public function generatePasswordHash(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}