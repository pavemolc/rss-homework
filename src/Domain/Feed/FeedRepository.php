<?php
declare(strict_types=1);

namespace App\Domain\Feed;

interface FeedRepository
{
    /**
     * @return array
     */
    public function getFeed(): array;
}