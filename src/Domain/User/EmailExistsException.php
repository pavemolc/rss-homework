<?php
declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\DomainException\DomainRecordExistsException;

class EmailExistsException extends DomainRecordExistsException
{
    public $message = 'User with such email is already registered.';
}