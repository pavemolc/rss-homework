<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\UserToken;

use App\Domain\User\User;
use App\Domain\UserToken\UserToken;
use App\Domain\UserToken\UserTokenRepository;

class InMemoryUserTokenRepository implements UserTokenRepository
{
    /**
     * @param User $user
     * @param string $token
     * @return bool
     */
    public function createUserToken(User $user, string $token): bool {
        $userToken= new UserToken();
        $userToken->token = $token;
        $userToken->user()->associate($user);

        return $userToken->save();
    }

    /**
     * @param string $token
     * @return User|null
     */
    public function getUserByToken(string $token): User {
        $userToken = UserToken::query()
            ->where('token', '=', $token)
            ->where('valid_till', '>', new \DateTime())
            ->first();

        return $userToken ? $userToken->user : null;
    }
}