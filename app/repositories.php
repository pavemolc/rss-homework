<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Domain\Feed\FeedRepository;
use App\Domain\UserToken\UserTokenRepository;
use App\Domain\Words\CommonWordRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use App\Infrastructure\Persistence\Feed\InMemoryRssFeedRepository;
use App\Infrastructure\Persistence\UserToken\InMemoryUserTokenRepository;
use App\Infrastructure\Persistence\Words\InMemoryCommonWordRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
        UserTokenRepository::class => \DI\autowire(InMemoryUserTokenRepository::class),
        FeedRepository::class => \DI\autowire(InMemoryRssFeedRepository::class),
        CommonWordRepository::class => \DI\autowire(InMemoryCommonWordRepository::class),
    ]);
};
