<?php
declare(strict_types=1);

namespace App\Domain\UserToken;

use App\Domain\User\User;

interface UserTokenRepository
{
    /**
     * @param User $user
     * @param string $token
     * @return bool
     */
    public function createUserToken(User $user, string $token): bool;

    /**
     * @param string $token
     * @return User
     */
    public function getUserByToken(string $token): User;
}