<?php
declare(strict_types=1);

namespace App\Application\Actions\Feed;

use App\Application\Actions\Action;
use App\Application\Services\WordCounter;
use App\Domain\Feed\FeedRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class FeedAction extends Action
{
    /** @var FeedRepository */
    protected $feedRepository;

    /** @var WordCounter */
    protected $wordCounter;

    /**
     * FeedAction constructor.
     * @param LoggerInterface $logger
     * @param FeedRepository $feedRepository
     * @param WordCounter $wordCounter
     */
    public function __construct(
        LoggerInterface $logger,
        FeedRepository $feedRepository,
        WordCounter $wordCounter
    ) {
        parent::__construct($logger);
        $this->feedRepository = $feedRepository;
        $this->wordCounter = $wordCounter;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $feed = $this->feedRepository->getFeed();
        $mostFrequentWords = $this->wordCounter->getMostFrequentWords($feed);

        return $this->respondWithData([
            'rssFeed' => $feed,
            'mostFrequentWords' => $mostFrequentWords
        ], 200);
    }
}