<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserRepository;

class InMemoryUserRepository implements UserRepository
{
    /**
     * {@inheritdoc}
     */
    public function findByEmail(string $email): ?User
    {
        $queryBuilder = User::query()->where('email', '=', $email);

        /** @var User $user */
        $user = $queryBuilder->first();
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function userEmailExists(string $email): bool
    {
        $queryBuilder = User::query()->where('email', '=', $email);
        return $queryBuilder->count() > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function createUser(string $email, string $passwordHash): bool
    {
        $user = new User();
        $user->email = $email;
        $user->password_hash = $passwordHash;
        return $user->save();
    }
}
