# RSS homework
## Prerequisites
1. Docker & Docker-compose
2. Composer

## How to run
1. `docker-compose up -d`
2. `composer install`
3. Check that `./logs` folder has writable permissions for everybody (didn't have enough time to make appropriate installation script)
4. Go to `localhost`

## How to run tests
```bash
composer test
```

## A few notes
* I've separated the implementation to 2 parts - API and front-end. The main reason behind this is that I don't really like any of PHP templating engines. Plus, the application feels more modular this way.
* For API part I've decided to use Slim V4. Last time I've used Slim, when it was still V2. Framework has changed since then. Now it encourages us to use Action-Domain-Responder pattern instead of MVC.
* Frontend is made using Vue JS. I've used CDN and didn't go through troubles installing Vue CLI or webpack. I though CDN would be enough for such small project.
* There is only two unit tests, although there isn't much to test :)