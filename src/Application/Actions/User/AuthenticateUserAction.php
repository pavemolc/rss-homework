<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Services\TokenGenerator;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use App\Domain\UserToken\UserTokenRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class AuthenticateUserAction extends UserAction
{
    /** @var UserRepository */
    protected $userRepository;

    /** @var UserTokenRepository */
    protected $userTokenRepository;

    /** @var TokenGenerator */
    protected $tokenGenerator;

    /**
     * AuthenticateUserAction constructor.
     * @param LoggerInterface $logger
     * @param UserRepository $userRepository
     * @param UserTokenRepository $userTokenRepository
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(
        LoggerInterface $logger,
        UserRepository $userRepository,
        UserTokenRepository $userTokenRepository,
        TokenGenerator $tokenGenerator
    ) {
        parent::__construct($logger, $userRepository);
        $this->userTokenRepository = $userTokenRepository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $params = $this->request->getParsedBody();
        $email = $params['email'];

        if (empty($email)) {
            throw new UserNotFoundException();
        }

        $user = $this->userRepository->findByEmail($email);

        if (empty($user) || empty($params['password']) || !password_verify($params['password'], $user->password_hash)) {
            throw new UserNotFoundException();
        }

        $token = $this->tokenGenerator->generateToken();
        $this->userTokenRepository->createUserToken($user, $token);

        $this->logger->info("User {$email} logged in");

        return $this->respondWithData([
            'apiToken' => $token
        ]);
    }
}