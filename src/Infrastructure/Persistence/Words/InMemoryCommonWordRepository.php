<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Words;

use App\Domain\Words\CommonWordRepository;
use http\Exception\RuntimeException;

class InMemoryCommonWordRepository implements CommonWordRepository
{
    const COMMON_WORD_MAX_COUNT = 50;

    /**
     * @return array
     */
    public function getMostCommonWords(): array
    {
        $endPoint = "https://en.wikipedia.org/w/api.php";
        $params = [
            "action" => "parse",
            "format" => "json",
            "prop" => "iwlinks",
            "page" => "Most_common_words_in_English"
        ];

        $url = $endPoint . "?" . http_build_query( $params );

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close( $ch );

        $result = json_decode($output, true);

        if (empty($result['parse']) || empty($result['parse']['iwlinks'])) {
            throw new RuntimeException("Couldn't fetch requested data from {$url}");
        }

        $result = $result['parse']['iwlinks'];
        $commonWords = [];

        foreach ($result as $key => $item) {
            if ($key >= self::COMMON_WORD_MAX_COUNT) {
                break;
            }

            $item = explode(':', $item['*']);
            $commonWords[] = $item[1];
        }

        return $commonWords;
    }
}