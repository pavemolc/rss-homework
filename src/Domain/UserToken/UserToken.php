<?php
declare(strict_types=1);

namespace App\Domain\UserToken;

use App\Domain\User\User;
use App\Infrastructure\Persistence\AbstractApiModel;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserToken extends AbstractApiModel
{

    const TOKEN = 'yabbadabba';

    /** @var string */
    protected $table = 'user_tokens';

    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = ['token', 'valid_till'];

    /**
     * UserToken constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->valid_till = new DateTime('+1 day');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}