const routes = [
    { path: '/registration', component: registrationForm },
    { path: '/login', component: loginForm },
    { path: '/rss-feed', component: rssFeed }
];

const router = new VueRouter({
    routes: routes,
    mode: 'history',
});

const app = new Vue({
    el: '#rss-app',
    router: router,
    data: {},
    mounted () {
        if (!this.isLoggedIn) {
            router.push('/login');
        }
    },
    computed: {
        isLoggedIn() {
            return $cookies.isKey('apiToken');
        }
    },
    methods: {}
});
