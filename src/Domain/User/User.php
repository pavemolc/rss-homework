<?php
declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\UserToken\UserToken;
use App\Infrastructure\Persistence\AbstractApiModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends AbstractApiModel
{
    /** @var string */
    protected $table = 'users';

    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = ['email', 'password_hash'];

    /**
     * @return HasMany
     */
    public function tokens(): HasMany
    {
        return $this->hasMany(UserToken::class);
    }
}
