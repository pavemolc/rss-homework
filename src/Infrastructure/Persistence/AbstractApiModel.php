<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

use Illuminate\Database\Eloquent\Model;
use JsonSerializable;

abstract class AbstractApiModel extends Model implements JsonSerializable
{
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}