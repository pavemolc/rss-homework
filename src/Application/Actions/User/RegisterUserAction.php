<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Services\TokenGenerator;
use App\Domain\User\EmailExistsException;
use App\Domain\User\UserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

class RegisterUserAction extends UserAction
{
    /** @var TokenGenerator */
    protected $tokenGenerator;

    /**
     * AuthenticateUserAction constructor.
     * @param LoggerInterface $logger
     * @param UserRepository $userRepository
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(
        LoggerInterface $logger,
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator
    ) {
        parent::__construct($logger, $userRepository);
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $params = $this->request->getParsedBody();

        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            throw new HttpBadRequestException($this->request, "'{$params['email']}' is not valid email address");
        }
        if (!$params['password'] || strlen($params['password']) < 6) {
            throw new HttpBadRequestException($this->request, 'Password must be at least 6 symbols long');
        }

        if ($this->userRepository->userEmailExists($params['email'])) {
            throw new EmailExistsException();
        }

        $passwordHash = $this->tokenGenerator->generatePasswordHash($params['password']);
        $this->userRepository->createUser($params['email'], $passwordHash);

        $this->logger->info("Users with email {$params['email']} was created.");

        return $this->respondWithData([
            'registered' => true
        ], 200);
    }
}