<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Domain\User\EmailExistsException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class ValidateEmailAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $params = $this->request->getParsedBody();

        $email = $params['email'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new HttpBadRequestException($this->request, "$email is not valid email address");
        }

        if ($this->userRepository->userEmailExists($email)) {
            throw new EmailExistsException();
        }

        return $this->respondWithData([], 200);
    }
}